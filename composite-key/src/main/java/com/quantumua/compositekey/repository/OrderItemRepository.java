package com.quantumua.compositekey.repository;

import com.quantumua.compositekey.composit.CompositeId;
import com.quantumua.compositekey.model.Order;
import com.quantumua.compositekey.model.OrderItem;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderItemRepository extends CrudRepository<OrderItem, CompositeId> {

    List<OrderItem> findByOrder(Order order);
    List<OrderItem> findByOrderId(Long orderId);
    List<OrderItem> findByOrderIdAndCustomerId(Long orderId, Long customerId);

}
