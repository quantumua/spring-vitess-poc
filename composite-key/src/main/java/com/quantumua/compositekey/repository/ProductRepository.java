package com.quantumua.compositekey.repository;

import com.quantumua.compositekey.model.Product;
import org.springframework.data.repository.CrudRepository;

public interface ProductRepository extends CrudRepository<Product, Long> {
}
