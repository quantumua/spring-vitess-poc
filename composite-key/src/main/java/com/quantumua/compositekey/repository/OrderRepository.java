package com.quantumua.compositekey.repository;

import com.quantumua.compositekey.composit.CompositeId;
import com.quantumua.compositekey.model.Order;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OrderRepository extends CrudRepository<Order, CompositeId> {
    List<Order> findBySku(String sku);
}
