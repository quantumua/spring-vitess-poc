package com.quantumua.compositekey.repository;

import com.quantumua.compositekey.model.Customer;
import org.springframework.data.repository.CrudRepository;

public interface  CustomerRepository extends CrudRepository<Customer, Long> {
}
