package com.quantumua.compositekey.dialect;

import org.hibernate.MappingException;
import org.hibernate.dialect.MySQL57Dialect;

public class VitessDialect extends MySQL57Dialect {

//    allocationSize in @SequenceGenerator and the number of requested from sequence table ids should be the same
//    otherwise we'll have errors due to the insert of duplicated ids. As the uniqueness is guaranteed only
//    in the scope of a single shard we also may have the same ids in different shards without throwing a "duplicate" exception.
    public static final int ALLOCATED_IDS_SIZE = 10;

    @Override
    public boolean supportsSequences() {
        return true;
    }

    @Override
    public boolean supportsPooledSequences() {
        return true;
    }

    @Override
    public String getSequenceNextValString(String sequenceName) throws MappingException {
        return "select next %d values from %s".formatted(ALLOCATED_IDS_SIZE, sequenceName);
    }
}
