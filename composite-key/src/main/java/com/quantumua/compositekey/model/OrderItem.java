package com.quantumua.compositekey.model;

import com.quantumua.compositekey.composit.CompositeId;
import com.quantumua.compositekey.dialect.VitessDialect;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.*;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "corder_item")
@IdClass(CompositeId.class)
public class OrderItem implements CompositedKey{

    public static final String ORDER_ITEM_GEN = "orderItemGen";

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = ORDER_ITEM_GEN)
    @SequenceGenerator(name = ORDER_ITEM_GEN, sequenceName ="corder_item_seq", allocationSize = VitessDialect.ALLOCATED_IDS_SIZE)
    private Long id;

    @Id
    @Column(name = "customer_id", updatable = false, nullable = false)
    Long customerId;


    @ManyToOne(optional = false, fetch = FetchType.EAGER)
    @Fetch(FetchMode.JOIN)
    @JoinColumnsOrFormulas(value = {
            @JoinColumnOrFormula(column = @JoinColumn(name = "order_id", referencedColumnName = "id")),
            @JoinColumnOrFormula(formula = @JoinFormula(value = "customer_id", referencedColumnName = "customer_id"))
    })
//in this case customer_id is repeated and need to be marked as updatable=false, insertable=false
// so the order_id have to be marked the same way as it is forbidden to mix changeable and non changeable fields in the same join
// as the result the order_id will be excluded from inserted fields and always will be null
//    @JoinColumns(
//            {
//                    @JoinColumn(updatable=false, insertable=false, nullable = false, name="order_id", referencedColumnName="id"),
//                    @JoinColumn(updatable=false, insertable=false, nullable = false, name="customer_id", referencedColumnName="customer_id")
//            }
//    )
    Order order;
    Long price;
    String sku;

}
