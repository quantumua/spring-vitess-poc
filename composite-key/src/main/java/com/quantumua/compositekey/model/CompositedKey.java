package com.quantumua.compositekey.model;

import org.springframework.data.domain.Persistable;

/**
 * Need to implement this interface by entity having composite key (CompositeId) to not do additional select before persisting the new entity.
 * With not null primary key Hibernate treats entity as not new. In our case compositeId is not null for the new entity as it  have to contain CompositeId.customerId - sharding key
 * Didn't use the right type Persistable<CompositeId> as required CompositeId getId() would clash with Long getId()
*/

public interface CompositedKey extends Persistable<Long> {

    @Override
    default boolean isNew(){
        return getId() == null;
    };
}
