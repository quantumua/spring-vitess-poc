package com.quantumua.compositekey.model;

import com.quantumua.compositekey.composit.CompositeId;
import com.quantumua.compositekey.dialect.VitessDialect;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "corder")
@IdClass(CompositeId.class)

public class Order implements CompositedKey {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_seq_generator")
    @SequenceGenerator(name = "order_seq_generator", sequenceName ="order_seq", allocationSize = VitessDialect.ALLOCATED_IDS_SIZE)
    private Long id;

    @Id
    @Column(name = "customer_id", updatable = false, nullable = false)
    Long customerId;

    private Long price;
    private String sku;
}
