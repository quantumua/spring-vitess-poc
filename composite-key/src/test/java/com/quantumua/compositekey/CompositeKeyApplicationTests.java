package com.quantumua.compositekey;

import com.quantumua.compositekey.composit.CompositeId;
import com.quantumua.compositekey.model.Customer;
import com.quantumua.compositekey.model.Order;
import com.quantumua.compositekey.model.OrderItem;
import com.quantumua.compositekey.repository.CustomerRepository;
import com.quantumua.compositekey.repository.OrderItemRepository;
import com.quantumua.compositekey.repository.OrderRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.util.NoSuchElementException;

@SpringBootTest
@ActiveProfiles("local")
@Slf4j
class CompositeKeyApplicationTests {

    @Autowired
    OrderRepository orderRepository;
    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    OrderItemRepository orderItemRepository;

    @Test
    public void test() {
        var customer = customerRepository.save(Customer.builder().email("maxtsyb@rambler.ru").build());
        System.out.println(customer);

        var order = Order.builder().customerId(customer.getId()).price(2L).build();
        Order saved = orderRepository.save(order);
        System.out.println(saved);

        var stored = orderRepository.findById(CompositeId.builder().id(saved.getId()).customerId(customer.getId()).build())
                .orElseThrow(NoSuchElementException::new);
        System.out.println(stored);

        stored.setSku("item");
        var updated = orderRepository.save(stored);
        System.out.println(updated);

        var orderItem1 = OrderItem.builder().order(stored).customerId(customer.getId()).sku("item 1").price(10L).build();
        var storedOrderItem1 = orderItemRepository.save(orderItem1);
        System.out.println(storedOrderItem1);

        var orderItem2 = OrderItem.builder().order(stored).customerId(customer.getId()).sku("item 2").price(20L).build();
        System.out.println(orderItemRepository.save(orderItem2));

        System.out.println(orderItemRepository.findByOrder(stored));
    }

}
