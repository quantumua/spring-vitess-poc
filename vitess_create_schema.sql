create table customer (
       customer_id bigint not null auto_increment,
        email varchar(255),
        primary key (customer_id)
    ) engine=InnoDB;	
create table corder (
       customer_id bigint not null,
        id bigint not null,
        price bigint,
        sku varchar(255),
        primary key (customer_id, id)
    ) engine=InnoDB;	
create table corder_item (
       customer_id bigint not null,
        id bigint not null,
        price bigint,
        sku varchar(255),
        order_id bigint not null,
        primary key (customer_id, id)
    ) engine=InnoDB;	
create table product (
       id bigint not null,
        description varchar(255),
        price bigint,
        sku varchar(255),
        primary key (id)
    ) engine=InnoDB;
create table corder_item_seq(id int, next_id bigint, cache bigint, primary key(id)) comment 'vitess_sequence';
insert into corder_item_seq(id, next_id, cache) values(0, 100, 100);
create table customer_seq(id int, next_id bigint, cache bigint, primary key(id)) comment 'vitess_sequence';
insert into customer_seq(id, next_id, cache) values(0, 100, 100);
create table order_seq(id int, next_id bigint, cache bigint, primary key(id)) comment 'vitess_sequence';
insert into order_seq(id, next_id, cache) values(0, 100, 100);
	